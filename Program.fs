﻿open System
open System.IO
open BorderlandsConfig

[<EntryPoint>]
let main argv =

    let configPath = @"G:\Donnees Vans\My Games\Borderlands The Pre-Sequel\WillowGame\SaveData\76561197961948532\profile.bin"
    let newSensi = 0x08

    printfn "Loading config path '%s'..." configPath

    let config =
        File.ReadAllBytes configPath
        |> BLConfig.loadConfig 0x1f9

    printfn "Config loaded"
    printfn "Current sensitivity is '%d'" config.Sensitivity

    let newConfig = BLConfig.changeSensitivity config (byte newSensi)
    let newFileContent = BLConfig.saveConfig newConfig
    File.WriteAllBytes(configPath, newFileContent)

    printfn "Sensitivity changed to '%d' successfully" newConfig.Sensitivity
    0 // return an integer exit code
