namespace BorderlandsConfig

module BLConfig =

    open System
    open System.IO
    open System.Security.Cryptography

    type BLConfig = {
        Hash : byte[];
        Content : byte[];
        Sensitivity : byte;
        SensitivityOffset : int;
    }

    let hashLength = 20

    let loadConfig (sensitivityOffset : int) (fileContent : byte[]) =
        let hash = fileContent.[0..hashLength - 1]
        let content = fileContent.[hashLength..]
        let sensitivity = fileContent.[sensitivityOffset]
        {Hash = hash; Content = content; Sensitivity = sensitivity; SensitivityOffset = sensitivityOffset;}

    let saveConfig (config: BLConfig) =
        Array.concat [config.Hash; config.Content]

    let changeSensitivity (config: BLConfig) (newSensi : byte) =
        let newContent = Array.copy config.Content
        newContent.[config.SensitivityOffset - hashLength] <- newSensi
        let sha = new SHA1CryptoServiceProvider();
        let newHash = sha.ComputeHash newContent
        {Hash = newHash; Content = newContent; Sensitivity = newSensi; SensitivityOffset = config.SensitivityOffset;}